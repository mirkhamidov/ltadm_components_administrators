<?
$items=array(
	"login" => array(
		"desc"				=> "Логин",
		"type"				=> "text",
		"maxlength"			=> "50",
		"size"				=> "30",
		"select_on_edit"	=> true,
		"js_validation"		=> array(
			"js_not_empty"	=> "Поле должно быть не пустым!",
			"js_match"		=> array (
				"pattern"	=> "^[A-Za-z0-9_\-]+$",
				"flags"		=> "g",
				"error"		=> "Только латинские символы, цифры и '_','-'!",
			),
		),
	),
	"password" => array(
		"desc" => "Пароль",
		"type" => "password",
		"maxlength" => "50",
		"size" => "30",
		"select_on_edit" => true,
		"copy_to_virtual" => "password2",
		"md5_value" => true,
		"js_validation" => array(
			"js_not_empty" => "Поле должно быть не пустым!",
			"js_match" => array (
				"pattern" => "^[A-Za-z0-9_\-]+$",
				"flags" => "g",
				"error" => "Только латинские символы, цифры и '_','-'!",
			),
		),
	),
  	"password2" => array(
		 "desc" => "Повторите пароль",
		 "type" => "password",
		 "maxlength" => "50",
		 "size" => "30",
		 "virtual" => true,
		 "not_in_table" => true,
		 "js_validation" => array(
		   "js_not_empty" => "Поле должно быть не пустым!",
		   "js_match" => array (
			 "pattern" => "^[A-Za-z0-9_\-]+$",
			 "flags" => "g",
			 "error" => "Только латинские символы, цифры и '_','-'!",
		   ),
		   "js_eq" => array (
			 "field" => "password",
			 "error" => "Значение поля должно совпадать с полем &quot;%password%&quot;!",
		   ),
		 ),
	   ),

  "home_dir" => array(
		 "desc" => "Домашняя директория",
		 "type" => "text",
		 "maxlength" => "30",
		 "size" => "20",
		 "js_validation" => array(
		   "js_not_empty" => "Поле должно быть не пустым!",
		   "js_match" => array (
			 "pattern" => "^[A-Za-z0-9_\-]+$",
			 "flags" => "g",
			 "error" => "Только латинские символы, цифры и '_','-'!",
		   ),
		 ),
	   ),

  "is_admin" => array(
		 "desc" => "Полный доступ к ресурсам системы?",
		 "type" => "radio",
		 "values" => array ("yes" => "Да", "no" => "Нет"),
		 "default_value" => "no",
		 "unique" => array("clear_all_to_default","set_for_this"),
	   ),


  "access_component" => array(
		 "desc" => "Доступ к разделам системы",
		 "type" => "access_items",
		 "size" => "30",
		 "serialize_value" => true,
		 "select_on_edit" => true,
		 "no_in_select_order_list" => true,
		 "no_in_select_cond_list" => true,
	   ),
  "access_dir" => array(
		 "desc" => "Доступ к домашним директориям других администраторов",
		 "type" => "access_dir",
		 "size" => "30",
		 "serialize_value" => true,
		 "no_in_select_order_list" => true,
		 "no_in_select_cond_list" => true,
	   ),
);
