<?
$config=array(
  "name" => "Администраторы сайта",
  "menu_icon" => "icon-user",
  "status" => "system",
  "windows" => array(
                      "create" => array("width" => 500,"height" => 600),
                      "edit" => array("width" => 600,"height" => 300),
                      "list" => array("width" => 600,"height" => 600),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_admins",
  "list" => array(
    "login" => array("isLink" => true),
    "home_dir" => array(),
    "is_admin" => array("align" => "center"),
  ),
  "select" => array(
     "default_orders" => array(
                           array("login" => "ASC")
                         ),
     "default" => array(
        "id_lt_admins" => array(
               "desc" => "Администратор",
               "type" => "select_from_table",
               "table" => "lt_admins",
               "key_field" => "id_lt_admins",
               "fields" => array("login"),
               "show_field" => "%1",
               "condition" => "",
               "order" => array ("login" => "ASC"),
               "use_empty" => true,
             ),

     ),
  ),
);

$actions=array(
  "create" => array(

    "before_code" => "create.php",

  ),
);

?>